note (1.3.22-2) unstable; urgency=medium

  * Bump debhelper compat to 11, no changes needed.
  * Bump Standards-version to 4.1.3, no changes needed.
  * Bump my copyright year to 2018 for the packaging.
  * Update Vcs-* to reflect the move to Salsa.
  * Explicitly suggest e2fsprogs (Closes: #887265).

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sat, 10 Mar 2018 22:20:42 -0600

note (1.3.22-1) unstable; urgency=medium

  * New upstream release.
    - Fix spurious error messages when TmpDir is not in a ext2fs
      (Closes: #495584).
  * Adopt the package (Closes: #866938).
  * Bump Standards-version to 4.0.1, no changes needed.
  * Bump debhelper compat version to 10.
  * Rewrite debian/copyright to use copyright-format 1.0.
  * Add Vcs-* fields in debian/control.
  * Correct bashism in code (Closes: #772284).
  * Change dependency on perl to perl:any to fix the multiarch hinter
    warnings.
  * Put all spelling corrections in fix-spelling.patch and add proper DEP-3
    headers.
  * Fix unescaped left brace in fix-unescaped-left-brace.patch causing
    note to be unusable with Perl 5.26 and on (Closes: #870636).

 -- Simon Quigley <tsimonq2@ubuntu.com>  Fri, 18 Aug 2017 16:13:12 -0500

note (1.3.7-1) unstable; urgency=low

  * New upstream release
  * Bump to Standards-Version 3.9.1 (no changes needed)
  * Simplified debian/rules file
  * Various cleanup in debian/
  * Added DEP-3 headers to debian/patches/*
  * Fixed spelling in manpages

 -- Alexandre De Dommelin <adedommelin@tuxz.net>  Sat, 12 Feb 2011 14:17:24 +0100

note (1.3.6-1) unstable; urgency=low

  * New upstream release (closes:#557012)
  * Fixed debian/watch thanks to patch provided by Ignace Mouzannar
  * Removed TODO file (no longer in upstream)
  * Added Misc-Depends to debian/control
  * Bump to Standard-Version from 3.8.1 to 3.8.4 (no changes needed)

 -- Alexandre De Dommelin <adedommelin@tuxz.net>  Mon, 08 Feb 2010 23:52:10 +0100

note (1.3.3-6) unstable; urgency=low

  * Fix TimeFormat bug using textDB, see timeformat.dpatch (closes: #495579)
  * Improve package description
  * Add debian/watch

 -- Alexandre De Dommelin <adedommelin@tuxz.net>  Sat, 13 Jun 2009 22:19:47 +0200

note (1.3.3-5) unstable; urgency=low

  * New maintainer (closes: #495597).
  * Fix bug in text dbdriver when no text::dbname value (closes: #495578).

 -- Alexandre De Dommelin <adedommelin@tuxz.net>  Fri, 12 Jun 2009 22:47:48 +0200

note (1.3.3-4) unstable; urgency=low

  * QA upload
  * Patch 02bashism: fix bashism in mysql/install.sh (Closes: #530151)
  * debian/copyright: point to versionend GPL file.
  * debian/rules: dh_clean -k => dh_prep
  * Standards-Vesion 3.8.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Thu, 28 May 2009 21:20:09 +0200

note (1.3.3-3) unstable; urgency=low

  * QA upload.
  * Setting maintainer to QA group as I orphaned this package

 -- Nico Golde <nion@debian.org>  Mon, 18 Aug 2008 21:31:01 +0200

note (1.3.3-2) unstable; urgency=low

  * Make Homepage a real control field.
  * Bump to standards version 3.8.0, no changes needed.
  * Bump compat level and debhelper dependency to 7.
  * Fix small errors in copyright to make lintian happy.
  * Add necessary comments to debian/patches/* files.
  * Move debhelper calls to binary-indep rules target as this package
    doesn't build any arch dependent data.

 -- Nico Golde <nion@debian.org>  Tue, 22 Jul 2008 19:13:38 +0200

note (1.3.3-1) unstable; urgency=low

  * New upstream release
    - fixed NOTEDB::mysql (and NOTEDB.pm) (Closes: #438175).

 -- Nico Golde <nion@debian.org>  Thu, 30 Aug 2007 15:23:52 +0200

note (1.3.1-5) unstable; urgency=low

  * Bumped compat level.
  * Changed maintainer address.
  * Added one additional space to Homepage tag.

 -- Nico Golde <nion@debian.org>  Sat, 31 Mar 2007 12:56:00 +0200

note (1.3.1-4) unstable; urgency=low

  * Bumped Standards-Version and fixed Build-Depends on dpatch and dephelper.
  * Removed some crap from rules.

 -- Nico Golde <nico@ngolde.de>  Mon, 31 Jul 2006 12:13:45 +0200

note (1.3.1-3) unstable; urgency=low

  * Fixed insecure tmp-file creation (closes: #337492).

 -- Nico Golde <nico@ngolde.de>  Sat,  5 Nov 2005 12:33:09 +0100

note (1.3.1-2) unstable; urgency=low

  * New Maintainer (closes: #329771).
  * Fixed manpage (closes: #292875).
  * Made a proper copyright file.
  * Added homepage tag to control file.
  * Removed menu file.

 -- Nico Golde <nico@ngolde.de>  Fri,  7 Oct 2005 15:57:27 +0200

note (1.3.1-1) unstable; urgency=low

  * new upstream version (closes: #228050)
  * added perl encryption packages to Suggest: (closes: #304969)
  * added other optional perl packages to Suggest:
  * reword copyright file (closes: #302974)

 -- Radovan Garabík <garabik@kassiopeia.juls.savba.sk>  Fri,  2 Sep 2005 20:30:22 +0200

note (1.1.1-2) unstable; urgency=low

  * added Build-Depends

 -- Radovan Garabik <garabik@melkor.dnp.fmph.uniba.sk>  Fri, 23 Feb 2001 21:35:53 +0100

note (1.1.1-1) unstable; urgency=low

  * Initial Release.

 -- Radovan Garabik <garabik@melkor.dnp.fmph.uniba.sk>  Sat, 23 Sep 2000 12:14:24 +0400
